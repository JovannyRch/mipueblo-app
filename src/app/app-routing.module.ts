import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'list',
    loadChildren: './list/list.module#ListPageModule'
  },
  { path: 'flora', loadChildren: './pages/flora/flora.module#FloraPageModule' },
  { path: 'fauna', loadChildren: './pages/fauna/fauna.module#FaunaPageModule' },
  { path: 'nomenclatura', loadChildren: './pages/nomenclatura/nomenclatura.module#NomenclaturaPageModule' },
  { path: 'geografia', loadChildren: './pages/geografia/geografia.module#GeografiaPageModule' },
  { path: 'ubicacion', loadChildren: './pages/ubicacion/ubicacion.module#UbicacionPageModule' },
  { path: 'clima', loadChildren: './pages/clima/clima.module#ClimaPageModule' },
  { path: 'poblacion', loadChildren: './pages/poblacion/poblacion.module#PoblacionPageModule' },
  { path: 'religion', loadChildren: './pages/religion/religion.module#ReligionPageModule' },
  { path: 'economia', loadChildren: './pages/economia/economia.module#EconomiaPageModule' },
  { path: 'cultura', loadChildren: './pages/cultura/cultura.module#CulturaPageModule' },
  { path: 'tradiciones', loadChildren: './pages/tradiciones/tradiciones.module#TradicionesPageModule' },
  { path: 'artesanias', loadChildren: './pages/artesanias/artesanias.module#ArtesaniasPageModule' },
  { path: 'gastronomia', loadChildren: './pages/gastronomia/gastronomia.module#GastronomiaPageModule' },
  { path: 'educacion', loadChildren: './pages/educacion/educacion.module#EducacionPageModule' },
  { path: 'extras', loadChildren: './pages/extras/extras.module#ExtrasPageModule' },
  { path: 'hechos', loadChildren: './pages/hechos/hechos.module#HechosPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
