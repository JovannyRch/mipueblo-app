import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NomenclaturaPage } from './nomenclatura.page';

describe('NomenclaturaPage', () => {
  let component: NomenclaturaPage;
  let fixture: ComponentFixture<NomenclaturaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NomenclaturaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NomenclaturaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
