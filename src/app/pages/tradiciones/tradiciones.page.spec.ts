import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TradicionesPage } from './tradiciones.page';

describe('TradicionesPage', () => {
  let component: TradicionesPage;
  let fixture: ComponentFixture<TradicionesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TradicionesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TradicionesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
