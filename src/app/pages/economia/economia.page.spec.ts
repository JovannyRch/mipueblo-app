import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EconomiaPage } from './economia.page';

describe('EconomiaPage', () => {
  let component: EconomiaPage;
  let fixture: ComponentFixture<EconomiaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EconomiaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EconomiaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
